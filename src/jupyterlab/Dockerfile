# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

# Ubuntu 20.04 (focal)
FROM ubuntu:focal

LABEL maintainer="Mark McCahill <mccahill@duke.edu>"
ARG NB_USER="jovyan"
ARG NB_UID="997"
ARG NB_GID="1003"

# Fix DL4006
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

USER root

# Install all OS dependencies for notebook server that starts but lacks all
# features (e.g., download as all possible file formats)
# - tini is installed as a helpful container entrypoint that reaps zombie
#   processes and such of the actual executable we want to start, see
#   https://github.com/krallin/tini#why-tini for details.
# - apt-get upgrade is run to patch known vulnerabilities in apt-get packages as
#   the ubuntu base image is rebuilt too seldom sometimes (less than once a month)
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update --yes && \
    apt-get upgrade --yes && \
    apt-get install --yes --no-install-recommends \
    tini \
    wget \
    ca-certificates \
    sudo \
    locales \
    fonts-liberation \
    software-properties-common \
    pwgen \
    sudo \
    net-tools \
    build-essential \
    libpng-dev \
    zlib1g-dev \
    libgl1-mesa-dev \
    libjpeg-dev \
    git \
    bzip2 \
    vim \
    jed \
    unzip \
    python3-dev \
    psmisc \
    libsm6 \
    emacs \
    inkscape \
    jed \
    libsm6 \
    libxext-dev \
    libxrender1 \
    lmodern \
    netcat \
    pandoc \
    python3-dev \
    python-dev \
    texlive-fonts-extra \
    texlive-fonts-recommended \
    texlive-latex-base \
    texlive-latex-extra \
    texlive-xetex \
    unzip \
    nano \
    krb5-multidev \
    python-setuptools \
    pkg-config \
    apt-transport-https \
    lsb-core \
    gfortran \
    libopenblas-dev \
    liblapack-dev \
    mpich \
    ffmpeg \
    libopenmpi-dev \
    dirmngr \
    gpg-agent \
    ssh \
    htop \
    tmux \
    libopenblas-dev \
    liblapack-dev \
    jq \
    run-one && \
    apt-get clean && rm -rf /var/lib/apt/lists/* && \
    echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen

# Configure environment
ENV CONDA_DIR=/opt/conda \
    SHELL=/bin/bash \
    NB_USER="${NB_USER}" \
    NB_UID=${NB_UID} \
    NB_GID=${NB_GID} \
    LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8
ENV PATH="${CONDA_DIR}/bin:${PATH}" \
    HOME="/home/${NB_USER}"

# Copy a script that we will use to correct permissions after running certain commands
COPY fix-permissions /usr/local/bin/fix-permissions
RUN chmod a+rx /usr/local/bin/fix-permissions

# Enable prompt color in the skeleton .bashrc before creating the default NB_USER
# hadolint ignore=SC2016
RUN sed -i 's/^#force_color_prompt=yes/force_color_prompt=yes/' /etc/skel/.bashrc && \
   # Add call to conda init script see https://stackoverflow.com/a/58081608/4413446
   echo 'eval "$(command conda shell.bash hook 2> /dev/null)"' >> /etc/skel/.bashrc

# Create NB_USER with name jovyan user with UID=997 and in the 'jovyan' group
# and make sure these dirs are writable by the `users` group.
RUN echo "auth requisite pam_deny.so" >> /etc/pam.d/su && \
    sed -i.bak -e 's/^%admin/#%admin/' /etc/sudoers && \
    sed -i.bak -e 's/^%sudo/#%sudo/' /etc/sudoers && \
    groupadd --gid $NB_GID jovyan && \
    useradd -l -m -s /bin/bash -N -u $NB_UID --gid $NB_GID $NB_USER && \
    mkdir -p "${CONDA_DIR}" && \
    chown "${NB_USER}:${NB_USER}" "${CONDA_DIR}" && \
    chmod g+w /etc/passwd && \
    fix-permissions "${HOME}" && \
    fix-permissions "${CONDA_DIR}"

USER ${NB_UID}
#ARG PYTHON_VERSION=default
ENV PYTHON_VERSION=3.8

# Setup work directory for backward-compatibility
RUN mkdir "/home/${NB_USER}/work" && \
    fix-permissions "/home/${NB_USER}"

# Install conda as jovyan and check the sha256 sum provided on the download site
WORKDIR /tmp

# CONDA_MIRROR is a mirror prefix to speed up downloading
# For example, people from mainland China could set it as
# https://mirrors.tuna.tsinghua.edu.cn/github-release/conda-forge/miniforge/LatestRelease
ARG CONDA_MIRROR=https://github.com/conda-forge/miniforge/releases/latest/download

# ---- Miniforge installer ----
# Check https://github.com/conda-forge/miniforge/releases
# Package Manager and Python implementation to use (https://github.com/conda-forge/miniforge)
# We're using Mambaforge installer, possible options:
# - conda only: either Miniforge3 to use Python or Miniforge-pypy3 to use PyPy
# - conda + mamba: either Mambaforge to use Python or Mambaforge-pypy3 to use PyPy
# Installation: conda, mamba, pip
RUN set -x && \
    # Miniforge installer
    miniforge_arch=$(uname -m) && \
    miniforge_installer="Mambaforge-Linux-${miniforge_arch}.sh" && \
    wget --quiet "${CONDA_MIRROR}/${miniforge_installer}" && \
    /bin/bash "${miniforge_installer}" -f -b -p "${CONDA_DIR}" && \
    rm "${miniforge_installer}" && \
    # Conda configuration see https://conda.io/projects/conda/en/latest/configuration.html
    conda config --system --set auto_update_conda false && \
    conda config --system --set show_channel_urls true && \
    if [[ "${PYTHON_VERSION}" != "default" ]]; then mamba install --quiet --yes python="${PYTHON_VERSION}"; fi && \
    mamba list python | grep '^python ' | tr -s ' ' | cut -d ' ' -f 1,2 >> "${CONDA_DIR}/conda-meta/pinned" && \
    # Using conda to update all packages: https://github.com/mamba-org/mamba/issues/1092
    conda update --all --quiet --yes && \
    conda clean --all -f -y && \
    rm -rf "/home/${NB_USER}/.cache/yarn" && \
    fix-permissions "${CONDA_DIR}" && \
    fix-permissions "/home/${NB_USER}"

# Using fixed version of mamba in arm, because the latest one has problems with arm under qemu
# See: https://github.com/jupyter/docker-stacks/issues/1539
RUN set -x && \
    arch=$(uname -m) && \
    if [ "${arch}" == "aarch64" ]; then \
        mamba install --quiet --yes \
            'mamba<0.18' && \
            mamba clean --all -f -y && \
            fix-permissions "${CONDA_DIR}" && \
            fix-permissions "/home/${NB_USER}"; \
    fi;

# Install Jupyter Notebook, Lab, and Hub
# Generate a notebook server config
# Cleanup temporary files
# Correct permissions
# Do all this in a single RUN command to avoid duplicating all of the
# files across image layers when the permissions change
RUN mamba install --quiet --yes \
    'notebook' \
    'jupyterhub' \
    'jupyterlab' && \
    mamba clean --all -f -y && \
    npm cache clean --force && \
    jupyter notebook --generate-config && \
    jupyter lab clean && \
    rm -rf "/home/${NB_USER}/.cache/yarn" && \
    fix-permissions "${CONDA_DIR}" && \
    fix-permissions "/home/${NB_USER}"

EXPOSE 8888

# Configure container startup
ENTRYPOINT ["tini", "-g", "--"]
CMD ["start-notebook.sh"]

# Copy local files as late as possible to avoid cache busting
COPY start.sh start-notebook.sh start-singleuser.sh /usr/local/bin/
# Currently need to have both jupyter_notebook_config and jupyter_server_config to support classic and lab
COPY jupyter_notebook_config.py /etc/jupyter/

# Fix permissions on /etc/jupyter as root
USER root

# Prepare upgrade to JupyterLab V3.0 #1205
RUN sed -re "s/c.NotebookApp/c.ServerApp/g" \
    /etc/jupyter/jupyter_notebook_config.py > /etc/jupyter/jupyter_server_config.py && \
    fix-permissions /etc/jupyter/

##################
# bitfusion client
RUN wget https://packages.vmware.com/bitfusion/ubuntu/20.04/bitfusion-client-ubuntu2004_4.5.0-4_amd64.deb
RUN apt-get update
RUN apt-get install -y ./bitfusion-client-ubuntu2004_4.5.0-4_amd64.deb
RUN rm bitfusion-client-ubuntu2004_4.5.0-4_amd64.deb
RUN bitfusion list_gpus

##################
# CUDA
RUN wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
RUN mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
RUN apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/7fa2af80.pub
RUN add-apt-repository "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/ /"
RUN apt-get update
RUN apt-get -y install cuda

#################
# cuDNN
ADD ./cuDNN_11.5  /cuDNN
RUN dpkg -i /cuDNN/cudnn-local-repo-ubuntu1804-8.3.1.22_1.0-1_amd64.deb
RUN apt-key add /var/cudnn-local-repo-*/7fa2af80.pub
RUN apt-get update
RUN apt-get install -y libcudnn8
RUN apt-get install -y libcudnn8-dev
RUN apt-get install -y libcudnn8-samples
RUN apt-get install -y libfreeimage3 libfreeimage-dev
RUN rm -r /cuDNN


##################
# pytorch
USER ${NB_UID}
#RUN pip3 install \
#    torch==1.10.1+cu113 \
#    torchvision==0.11.2+cu113 \
#    torchaudio==0.10.1+cu113 \
#    -f https://download.pytorch.org/whl/cu113/torch_stable.html
RUN pip3 install \
    torch \
    torchvision \
    torchaudio
USER root

#################
# tensorflow
RUN mamba install -y tensorflow-gpu

################
#  nvdashboard
RUN mamba install -y  -c conda-forge jupyterlab-nvdashboard


###############
# misc python
RUN mamba install -y \
    'conda-forge::blas=*=openblas' \
    'pandas' \
    'numexpr' \
    'matplotlib' \
    'scipy' \
    'seaborn' \
    'scikit-learn' \
    'scikit-image' \
    'sympy' \
    'cython' \
    'patsy' \
    'statsmodels' \
    'cloudpickle' \
    'dill' \
    'numba' \
    'bokeh' \
    'sqlalchemy' \
    'hdf5' \
    'h5py' \
    'vincent' \
    'beautifulsoup4' \
    'protobuf' \
    'xlrd' \
    'numpy' 

RUN mamba install -y \
    ipympl \
    phantomjs \
    pillow \
    selenium \
    'nomkl' \
    'imageio' \
    'bleach' \
    'html5lib' \
    'pandas-datareader' \
    'psutil' \
    'ipyparallel' \
    'pybind11' \
    'pytables' 

################
# Rapids.ai and cuGraph
RUN mamba install -y -c rapidsai -c nvidia -c conda-forge rapids=21.12 python=3.8 cudatoolkit=11.2 dask-sql 
RUN mamba install -y -c pyviz holoviews 


################
# FinTech540
RUN mamba install -y \
    fastparquet \
    graphviz \
    gym \
    linearmodels \
    natsort \
    numpy-indexed \
    openpyxl \
    pandas-profiling \
    plotly \
    pyarrow \
    pycurl \
    pydocstyle \
    pydotplus \
    scikit-learn-intelex \
    ta-lib \
    xlsxwriter \
    xlwings \
    xlwt \
    xmltodict \
    networkx \
    yfinance \
    regex \
    tqdm \
    urllib3 
  
RUN pip install  arch
RUN pip install  git+https://github.com/scikit-portfolio/scikit-portfolio
RUN pip install  git+https://github.com/RJT1990/pyflux 

RUN pip install graphviz 
RUN mamba install -c plotly plotly
RUN mamba install -c conda-forge -c plotly jupyter-dash
RUN jupyter lab build		

# spaCy
RUN  mamba install -c conda-forge spacy
RUN  pip install spacy-transformers
RUN  pip install spacy-lookups-data
RUN  python -m spacy download en_core_web_sm

############## end FinTech 540 ################


RUN     fix-permissions "${CONDA_DIR}"


USER root
RUN apt-get install -y \
    nfdump \
	ninja-build

##################
# Switch back to jovyan to avoid accidental container runs as root
USER ${NB_UID}

WORKDIR "${HOME}/work"
